package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {
    protected final List<E> entities = new ArrayList<>();


    public List<E> findAll(String userId) {
        final List<E> pro = new ArrayList<>();
        for (E entity : entities) {
            if (entity == null) continue;
            if (entity.getUserId().equals(userId)) pro.add(entity);
        }
        return pro;
    }

    @Override
    public List<E> findAll(String userId, Comparator comparator) {
        final List<E> entity = new ArrayList<>(findAll(userId));
        entity.sort(comparator);
        return entity;
    }

    @Override
    public E findById(String userId, String id) {
        if (id == null) return null;
        for (E entity : entities) {
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeById(String userId, String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }
}
