package ru.tsc.karbainova.tm.api.entity;

import java.util.Date;

public interface IHasDateCreated {
    Date getCreated();

    void setCreated(Date created);
}